const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class DiscountedSA extends Model {}

DiscountedSA.init({
    Amount:{
        type: DataTypes.FLOAT,
        defaultValue: 0
    },
    ValueDate: {
        type: DataTypes.DATEONLY,
        defaultValue: DataTypes.NOW
    },
    FinalDate: {
        type: DataTypes.DATEONLY,
        require: true
    },
    InterestRate: DataTypes.FLOAT,
    TotalIntAmount: DataTypes.INTEGER,
    WorkingAccount: DataTypes.TEXT,
    WorkingAccountRefID: DataTypes.TEXT,
    CustomerName: DataTypes.TEXT,
    AmountLCY: {
        type: DataTypes.FLOAT,
        defaultValue: 0
    },
    AmountFCY:{
        type: DataTypes.FLOAT,
        defaultValue: 0
    },
    AmountLCYInterest: {
        type: DataTypes.FLOAT,
        defaultValue: 0
    },
    AmountFCYInterest: {
        type: DataTypes.FLOAT,
        defaultValue: 0
    },
    NarrativeInterest: DataTypes.TEXT,
    Narrative: DataTypes.TEXT,
    DealRate: DataTypes.FLOAT,
    Teller: DataTypes.TEXT,
    EcxhRate: DataTypes.FLOAT,
    CustBal: DataTypes.FLOAT,
    AmtPaid: DataTypes.FLOAT,
    Currency: DataTypes.INTEGER,
    PaymentCurrency: DataTypes.INTEGER,
    DebitAccount: DataTypes.TEXT,
    CreditAccount: DataTypes.TEXT
}, {sequelize, modelName:'DISCOUNTED_SA'})

module.exports = DiscountedSA