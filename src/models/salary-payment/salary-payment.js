const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class SalaryPayment extends Model {}

SalaryPayment.init({
    RefID: DataTypes.TEXT,
    //Company Information
    CompanyRefID: DataTypes.TEXT,
    CompanyName: DataTypes.TEXT,
    Currency: DataTypes.INTEGER,
    //Company Account Information
    CompanyAccountRefID: DataTypes.TEXT,
    //Payment Information
    TotalDebitAmt: DataTypes.FLOAT,
    FrequencyDate: DataTypes.DATEONLY,
    Term: {
        type: DataTypes.ENUM("None", "Week", "Month"), //Non_Prequency => None
        defaultValue: "None"
    },
    EndDate: DataTypes.DATEONLY,
    OrderingCust: DataTypes.TEXT
}, {sequelize, modelName:'SALARY_PAYMENT', timestamps: true})

module.exports = SalaryPayment