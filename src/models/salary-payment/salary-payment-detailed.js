const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class SalaryPaymentDetailed extends Model {}

SalaryPaymentDetailed.init({
    ReceiverAccountRefID: DataTypes.TEXT, // Ref ID
    ReceiverName: DataTypes.TEXT,
    CompanyAccountRefID: DataTypes.TEXT, // Ref ID
    Amount: DataTypes.FLOAT,
    ReceiveDate: DataTypes.DATEONLY
}, {sequelize, modelName:'SALARY_PAYMENT_DETAILED', timestamps: true})

module.exports = SalaryPaymentDetailed