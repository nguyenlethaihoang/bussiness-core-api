const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class ForeignExchange extends Model {}

ForeignExchange.init({
    RefID: DataTypes.TEXT,
    CustomerName: DataTypes.TEXT,
    Address: DataTypes.TEXT,
    PhoneNo: DataTypes.TEXT,
    TellerIDst: DataTypes.TEXT,
    DebitAmtLCY: DataTypes.FLOAT,
    DebitAmtFCY: DataTypes.FLOAT,
    DebitDealRate: DataTypes.FLOAT,
    TellerIDnd: DataTypes.TEXT,
    CreditDealRate: DataTypes.FLOAT,
    AmountPaidToCust: DataTypes.FLOAT,
    Narrative: DataTypes.TEXT,
    DebitCurrencyID: DataTypes.INTEGER,
    CurrencyPaidID: DataTypes.INTEGER,
    DebitAccount: DataTypes.INTEGER,
    CreditAccount: DataTypes.INTEGER,
    ChargeCollectionID: {
        type: DataTypes.INTEGER,
        allowNull: true
    }
}, {sequelize, modelName:'FOREIGNEXCHANGE'})

module.exports = ForeignExchange