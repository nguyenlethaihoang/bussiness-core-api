const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class ChequeStopReason extends Model {}

ChequeStopReason.init({
    Name: DataTypes.TEXT,
    Value: {
        type: DataTypes.TEXT,
        defaultValue: 0
    }
}, {sequelize, modelName:'CHEQUE_STOP_REASON', timestamps: false})
module.exports = ChequeStopReason