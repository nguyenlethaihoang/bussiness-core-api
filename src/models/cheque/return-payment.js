const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class ChequeReturn extends Model {}

ChequeReturn.init({
    ChequeID: {
        type: DataTypes.TEXT
    },
    //Owner Information
    CustomerName:DataTypes.TEXT,
    DocID: DataTypes.TEXT,
    WorkingAccount: DataTypes.TEXT,
    CustomerID: DataTypes.TEXT, // RefID
    //Stop Information
    ChequeNo: DataTypes.INTEGER,
    ReturnedDate: DataTypes.DATEONLY,
    IsReturned: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    Narrative: DataTypes.TEXT

}, {sequelize, modelName:'CHEQUE_RETURN'})

module.exports = ChequeReturn