const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class ChequeItem extends Model {}

ChequeItem.init({
    ChequeID: {
        type: DataTypes.TEXT
    },
    ChequeStatus: {
        type: DataTypes.ENUM('Available', 'Used', 'Stopped', 'Canceled'),
        defaultValue: 'Available'
    },
    ChequeNo: DataTypes.INTEGER,
}, {sequelize, modelName:'CHEQUEITEM'})

module.exports = ChequeItem