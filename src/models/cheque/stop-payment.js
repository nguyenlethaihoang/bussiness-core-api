const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = require('../../database/sequelize')

class ChequeStop extends Model {}

ChequeStop.init({
    ChequeID: {
        type: DataTypes.TEXT
    },
    //Owner Information
    CustomerName:DataTypes.TEXT,
    Currency: DataTypes.INTEGER,
    DocID: DataTypes.TEXT,
    WorkingAccount: DataTypes.TEXT,
    CustomerID: DataTypes.TEXT, // RefID
    //Stop Information
    ChequeNoStart: DataTypes.INTEGER,
    ChequeNoEnd: DataTypes.INTEGER,
    ItemsQuantity: DataTypes.INTEGER, // So to Sec bi stop
    FromAmount: DataTypes.FLOAT,
    ToAmount: DataTypes.FLOAT,
    WaiveCharges: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    StoppedDate: DataTypes.DATEONLY,
    //Cancel Stop Information
    IsStopped: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    CancelStopDate: DataTypes.DATEONLY

}, {sequelize, modelName:'CHEQUE_STOP'})

module.exports = ChequeStop