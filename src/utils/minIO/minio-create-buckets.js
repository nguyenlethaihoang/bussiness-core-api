
const minioClient = require('./minio-connect')
const bucketName_Image = process.env.SIGNATURE_BUCKET_NAME
const bucketName_Doc = process.env.DOCUMENT_BUCKET_NAME
const minIOConnect = {
    connect: async () => {
          await minioClient.makeBucket(bucketName_Image, "hello-there").catch((e) => {
            console.log(
              `Error while creating bucket '${bucketName_Image}': ${e.message}`
             );
          });
          await minioClient.makeBucket(bucketName_Doc, "hello-there").catch((e) => {
            console.log(
              `Error while creating bucket '${bucketName_Doc}': ${e.message}`
             );
          });
        
          console.log(`Listing all buckets...`);
          const bucketsList = await minioClient.listBuckets();
          console.log(
            `Buckets List: ${bucketsList.map((bucket) => bucket.name).join(",\t")}`
          );
        }
}
module.exports = minIOConnect

// (async () => {
//   console.log(`Creating Bucket: ${bucketName}`);
//   await minioClient.makeBucket(bucketName, "hello-there").catch((e) => {
//     console.log(
//       `Error while creating bucket '${bucketName}': ${e.message}`
//      );
//   });

//   console.log(`Listing all buckets...`);
//   const bucketsList = await minioClient.listBuckets();
//   console.log(
//     `Buckets List: ${bucketsList.map((bucket) => bucket.name).join(",\t")}`
//   );
// })();