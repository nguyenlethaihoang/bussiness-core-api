const DebitAccountModel = require('../models/account/debitAccount')
const CustomerModel = require('../models/customer/customer')
const salaryPaymentModel = require('../models/salary-payment/salary-payment')
const salaryPaymentDetailedModel = require('../models/salary-payment/salary-payment-detailed')
const readXlsxFile = require('read-excel-file/node')
const { Op } = require('sequelize')



const salaryPaymentController = {
    uploadFile: async (req, res, next) => {
        try{
            let jsonRows = []
            // GET FILE
            const file = req.file
            if(!file)
                throw "Provide .xlsx file"
            //File Path
            await  readXlsxFile(file.buffer).then(async(rows) => {
                rows.map((row, index) => {
                    if(index >= 1){
                        let jsonRow = {
                            Account: row[0],
                            ReceiverName: row[1],
                            Amount: row[2]
                        }
                        console.log(jsonRow)
                        jsonRows.push(jsonRow)
                    }
                })
            })
            console.log(jsonRows)
            return res.status(200).json({
                message: "Upload File Successfully",
                data: jsonRows
            })

        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    create: async (req, res, next) => {
        try{
            const paymentReq = {
                companyAccount: req.body.companyAccount, // REF ID
                currency: req.body.currency,
                totalDebitAmt: req.body.totalDebitAmt,
                frequencyDate: req.body.frequencyDate,
                term: req.body.term,
                endDate: req.body.endDate,
                orderingCust: req.body.orderingCust,
                creditAccount: req.body.creditAccount // Array: [{Account: "0700...", Amount: 1000000}, {}, {}]
            }

            // CHECK REQUEST
            if(!paymentReq.companyAccount){
                throw "Company Account is required"
            }

            // CHECK COMPANY 
            const companyAccountDB = await DebitAccountModel.findOne({
                where: {
                    Account: paymentReq.companyAccount
                },
                include: [{
                    model: CustomerModel, as: "Customer"
                }]
            })

            if(!companyAccountDB)
                throw "Company Account is not found"
            const customerType = companyAccountDB.get("Customer").CustomerType
            if(customerType !== 2){
                throw "Account is not Company_Account"
            }

            let failedAccount = []
            let successfullAccount = []
            // CREATE SALARY_PAYMENT
            const newPayment = await salaryPaymentModel.create({
                RefID: "",
                CompanyRefID: companyAccountDB.get("Customer").RefID,
                CompanyName: companyAccountDB.get("Customer").GB_FullName,
                Currency: paymentReq.currency,
                CompanyAccountRefID: paymentReq.companyAccount,
                TotalDebitAmt: paymentReq.totalDebitAmt,
                FrequencyDate: paymentReq.frequencyDate,
                Term: paymentReq.term,
                EndDate: paymentReq.endDate,
                OrderingCust: paymentReq.orderingCust,
                CompanyAccountID: companyAccountDB.getDataValue("id"),
                Status: 1
            })

            if(!newPayment){
                throw "Create Salary_Payment_Record Failed"
            }
            const paymentID = newPayment.getDataValue("id")

            //UPDATE REF ID
            let refTemp = paymentID.toString().padStart(6, '0')
            const refID = `TT.23030.${refTemp}`
            newPayment.update({
                RefID: refID
            })

            // CHECK CREDIT INFORMATION
            if(paymentReq.creditAccount){
                await Promise.all(paymentReq.creditAccount.map( async (value, index) => {
                    if(!value.Account || !value.Amount){
                        failedAccount.push(value)
                    }else{
                        console.log(value)
                        let creditAccountDB = await DebitAccountModel.findOne({
                            where: {
                                Account: value.Account
                            },
                            include: [{
                                model: CustomerModel, as: "Customer"
                            }]
                        })
                        if(!creditAccountDB){
                            failedAccount.push(value)
                        }else{
                            let newRecord = await salaryPaymentDetailedModel.create({
                                ReceiverAccountRefID: value.Account,
                                ReceiverName: creditAccountDB.get("Customer").GB_FullName,
                                CompanyAccountRefID: paymentReq.companyAccount,
                                Amount: value.Amount || 0.00,
                                ReceiveDate: paymentReq.frequencyDate,
                                PaymentID: paymentID,
                                ReceiverAccountID: creditAccountDB.getDataValue("id")
                            })

                            if(!newRecord){
                                failedAccount.push(value)
                            }else{
                                successfullAccount.push(value)
                            }
                        }
                    }
                }))
            }

            return res.status(200).json({
                message: "Create Salary_Payment successfully",
                data: {
                    Payment: newPayment,
                    Failed: failedAccount,
                    Successfull: successfullAccount
                }
            })

        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },

    validate: async (req, res, next) => {
        try{
            const paymentID = req.params.ID
            const statusReq = req.body.status
            if(!paymentID){
                throw "Payment_ID is required"
            }
            const paymentDB = await salaryPaymentModel.findByPk(paymentID)
            if(!paymentDB)
                throw "Salary_Payment_Record is not found"
            
            //CHECK STATUS
            const statusDB = paymentDB.getDataValue("Status")
            if(statusDB != 1)
                throw "Validated"
            
            const updatedPayment = await paymentDB.update({
                Status: statusReq
            })

            return res.status(200).json({
                message: "Validate Successfully",
                data: updatedPayment
            })

        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    getID: async(req, res, next) => {
        try{
            const paymentID = req.params.ID

            if(!paymentID)
                throw "Salary_Payment_ID is required"

            // GET SALARY PAYMENT RECORD
            const paymentDB = await salaryPaymentModel.findByPk(paymentID)
            if(!paymentDB)
                throw "Salary_Payment_Record not found"
            
            // GET DETAIL SALARY PAYMENT
            const detailedItems = await salaryPaymentDetailedModel.findAll({
                where: {
                    PaymentID: paymentID
                },
                attributes: ["ReceiverAccountRefID", "ReceiverName", "Amount"]
            })

            return res.status(200).json({
                message: "Get Salary Payment Record Successfully",
                data: {
                    SalaryPayment: paymentDB,
                    Receivers: detailedItems
                }
            })
            
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    enquiry: async (req, res, next) => {
        try{
            const enquiryReq = {
                refID: req.body.refID,
                companyRefID: req.body.companyRefID,
                companyName: req.body.companyName,
                currency: req.body.currency,
                companyAccount: req.body.companyAccount, // account Ref ID
                term: req.body.term,
                frequencyDate: req.body.frequencyDate,
                endDate: req.body.endDate
            }

            let condition = {}
            if(enquiryReq.refID)
                condition.RefID = {[Op.substring]: enquiryReq.refID}
            if(enquiryReq.companyRefID)
                condition.CompanyRefID = {[Op.substring]: enquiryReq.companyRefID}
            if(enquiryReq.companyName)
                condition.CompanyName = {[Op.substring]: enquiryReq.companyName}
            if(enquiryReq.currency)
                condition.Currency = enquiryReq.currency
            if(enquiryReq.companyAccount)
                condition.CompanyAccountRefID = {[Op.substring]: enquiryReq.companyAccount}
            if(enquiryReq.term)
                condition.Term = enquiryReq.term
            if(enquiryReq.frequencyDate)
                condition.FrequencyDate = enquiryReq.frequencyDate
            if(enquiryReq.endDate)
                condition.EndDate = enquiryReq.endDate

            const paymentListDB = await salaryPaymentModel.findAll({
                where: condition,
                include: [{
                    model: DebitAccountModel,
                    include: [{
                        model: CustomerModel, as: "Customer"
                    }]
                }]
            })

            if(!paymentListDB)
                throw "Enquiry Salary_Payment List Failed"
            return res.status(200).json({
                message: "Enquiry Salary_Payment List Successfully",
                data: paymentListDB
            })
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    }

}

module.exports = salaryPaymentController