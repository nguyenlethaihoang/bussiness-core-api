const chequeModel = require('../../models/cheque/cheque')
const chequeItemModel = require('../../models/cheque/chequeItem')
const chequeStopReasonModel = require('../../models/storage/cheque-stop-reason')
const chequeStopModel = require('../../models/cheque/stop-payment')
const debitAccountModel = require('../../models/account/debitAccount')
const customerModel = require('../../models/customer/customer')
const { Op } = require('sequelize')
const chequeStopController = {
    stopPayment: async (req, res, next) => {
        try{
            const stopReq = {
                chequeID: req.body.chequeID,
                chequeNoStart: req.body.chequeNoStart,
                chequeNoEnd: req.body.chequeNoEnd,
                itemsQuantity: req.body.itemsQuantity,
                fromAmount: req.body.fromAmount,
                toAmount: req.body.toAmount,
                waiveCharges: req.body.waiveCharges,
                stoppedDate: req.body.stoppedDate,
                reason: req.body.stopReason
            }

            if(!stopReq.chequeID || !stopReq.chequeNoStart || !stopReq.chequeNoEnd || !stopReq.reason){
                throw "ChequeID, Cheque_No_Start, Cheque_No_End, Cheque_Stop_Reason are required"
            }
    
    
            // CHECK CHEQUE ID
            const chequeDB = await chequeModel.findOne({
                where: {
                    ChequeID: stopReq.chequeID
                }
            })
            if(!chequeDB){
                throw "Cheque ID Not Found"
            }
            // CHECK CHEQUE STATUS
            const statusDB = chequeDB.getDataValue("Status")
            if(statusDB !== 2)
                throw "Invalid Cheque"

            // CHECK CHEQUE NO 
            const chequeNoStartDB = chequeDB.getDataValue('ChequeNoStart')
            const chequeNoEndDB = chequeDB.getDataValue('ChequeNoEnd')
            const quantityDB = chequeDB.getDataValue('IssuedQuantity')
            if(chequeNoStartDB > stopReq.chequeNoStart || chequeNoEndDB < stopReq.chequeNoEnd || quantityDB < stopReq.itemsQuantity){
                throw "Cheque_No Invalid"
            }

            // GET DETAIL INFORMATION
            // Working Account
            const workingAccountDB = await debitAccountModel.findOne({
                where: {
                    id: chequeDB.getDataValue('WorkingAccount')
                },
                include: {
                    model: customerModel, as: 'Customer'
                }
            })
            if(!workingAccountDB){
                throw "Cheque's Working Account Critical Error Occur"
            }
            
    
            // CREATE STOP RECORD DATABASE
            const newStopPayment = await chequeStopModel.create({
                ChequeID: stopReq.chequeID,
                CustomerName: workingAccountDB.get('Customer').GB_FullName,
                Currency: chequeDB.getDataValue('Currency'),
                DocID: workingAccountDB.get('Customer').DocID,
                WorkingAccount: workingAccountDB.getDataValue("Account"),
                CustomerID: workingAccountDB.get('Customer').RefID,
                ChequeNoStart: stopReq.chequeNoStart,
                ChequeNoEnd: stopReq.chequeNoEnd,
                ItemsQuantity: stopReq.itemsQuantity,
                FromAmount: stopReq.fromAmount || 0.00,
                ToAmount: stopReq.toAmount || 0.00,
                WaiveCharges: stopReq.waiveCharges,
                StoppedDate: stopReq.stoppedDate,
                IsStopped: true,
                CancelStopDate: null,
                StopReason: stopReq.reason,
                Status: 1
            })

            if(!newStopPayment){
                throw "Stop Failed"
            }

            return res.status(200).json({
                message: "Stop Payment Successfully",
                data: newStopPayment
            })
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
        
    },
    getStopPaymentID: async (req, res, next) => {
        try{
            const stopIDReq = req.params.stopID
            if(!stopIDReq){
                throw "Stop_Payment_ID is required"
            }

            const stopPaymentDB = await chequeStopModel.findByPk(stopIDReq)
            if(!stopIDReq){
                throw "Stop_Payment Information is not found"
            }

            return res.status(200).json({
                message: "Get Stop Payment Information Successfully",
                data: stopPaymentDB
            })

        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
        
    },
    validate: async (req, res, next) => {
        try{
            // STOP 
            const stopIDReq = req.params.stopID
            const statusReq = req.body.status

            // CHECK REQUEST
            if(!stopIDReq){
                throw "Stop_Payment_ID is required"
            }
            if(!stopIDReq){
                throw "Status is required"
            }

            // CHECK STATUS
            let itemsStopped = 0
            const stopDB = await chequeStopModel.findByPk(stopIDReq)
            if(!stopDB)
                throw "Stop_Payment Record is not found"
            
            const statusDB = stopDB.getDataValue("Status")
            if(statusDB != 1){
                throw "Validated"
            }else{
                if(statusReq == 2){
                    const chequeNoStartDB = stopDB.getDataValue("ChequeNoStart")
                    const chequeNoEndDB = stopDB.getDataValue("ChequeNoEnd")
                    const chequeIDDB = stopDB.getDataValue('ChequeID')
                    const updatedChequeItems = await chequeItemModel.update({
                        ChequeStatus: "Stopped"
                    }, {
                        where: {
                            ChequeID: chequeIDDB,
                            ChequeStatus: "Available",
                            ChequeNo: {[Op.between]: [chequeNoStartDB, chequeNoEndDB]}
                        }
                    })

                    console.log(updatedChequeItems)
                    if(!updatedChequeItems){
                        throw "Validate Failed"
                    }

                    itemsStopped = updatedChequeItems[0]
                }else if(statusReq == 3){
                    
                }else{
                    throw "Status Invalide"
                }
            }

            const updatedCheque = await stopDB.update({
                Status: statusReq
            })

            return res.status(200).json({
                message: "Validate successfully",
                data: {
                    StopPayment: updatedCheque,
                    StopItems: itemsStopped
                }
            })

        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
        
    },
    // enquiry Stop Payment
    enquiry: async (req, res, next) => {
        try{
            const enquiryReq = {
                chequeID: req.body.chequeID,
                docID: req.body.docID,
                customerID: req.body.customerID,
                chequeType: req.body.chequeType,
                workingAccount: req.body.workingAccount,
                customerName: req.body.customerName,
                stoppingReason: req.body.reason,
                chequeNo: req.body.chequeNo,
                stoppedDate: req.body.stoppedDate
            }
            if(enquiryReq.chequeType == "AB"){
                return res.status(200).json({
                    message: "Enquiry Stop_Payment Sucessfully",
                    data: []
                })
            }else{
                let enquiryCondition = {}
                if(enquiryReq.docID)
                    enquiryCondition.DocID = {[Op.substring]: enquiryReq.docID}
                if(enquiryReq.customerID)
                    enquiryCondition.CustomerID = {[Op.substring]: enquiryReq.customerID}
                
                if(enquiryReq.workingAccount)
                    enquiryCondition.WorkingAccount = {[Op.substring]: enquiryReq.workingAccount}
                if(enquiryReq.customerName)
                    enquiryCondition.CustomerName = {[Op.substring]: enquiryReq.customerName}
                if(enquiryReq.stoppingReason)
                    enquiryCondition.StopReason = enquiryReq.stoppingReason
                if(enquiryReq.chequeNo){
                    enquiryCondition.ChequeNoStart = {[Op.lte]: [enquiryReq.chequeNo]}
                    enquiryCondition.ChequeNoEnd = {[Op.gte]: [enquiryReq.chequeNo]}
                }
                    
                if(enquiryReq.stoppedDate)
                    enquiryCondition.StoppedDate = enquiryReq.stoppedDate
                if(enquiryReq.chequeID)
                    enquiryCondition.ChequeID = {[Op.substring]: enquiryReq.chequeID}
                enquiryCondition.IsStopped = true

                console.log(enquiryCondition)

                const stopDB = await chequeStopModel.findAll({
                    where: enquiryCondition,
                    include: [{
                            model: chequeStopReasonModel
                        }]
                })

                if(!stopDB){
                    throw "Enquiry Failed"
                }

                return res.status(200).json({
                    message: "Enquiry Stop_Payment Sucessfully",
                    data: stopDB
                })
            }
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    cancelStopPayment: async (req, res, next) => {
        try{
            const cancelReq = {
                cancelDate: req.body.cancelDate,
            }
            const stopIDReq = req.params.stopID
            // CHECK REQUEST
            if(!stopIDReq){
                throw "Stop_Payment ID is required"
            }
            // CHECK CHEQUE_STOP RECORD
            const stopDB = await chequeStopModel.findByPk(stopIDReq)
            if(!stopDB){
                throw "Stop_Payment_Record is not found"
            }
            const statusDB = stopDB.getDataValue("Status")
            const isStopDB = stopDB.getDataValue("IsStopped")
            if(!isStopDB)
                throw "Record was canceled"

            // UPDATE CHEQUEITEM
            const chequeNoStartDB = stopDB.getDataValue("ChequeNoStart")
            const chequeNoEndDB = stopDB.getDataValue("ChequeNoEnd")
            const chequeIDDB = stopDB.getDataValue('ChequeID')
            const updatedChequeItems = await chequeItemModel.update({
                ChequeStatus: "Available"
            }, {
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Stopped",
                    ChequeNo: {[Op.between]: [chequeNoStartDB, chequeNoEndDB]}
                }
            })
            let updatedStop
            // UPDATE CHEQUE - IS_STOPPED
            if(statusDB == 1){
                updatedStop = await stopDB.update({
                    CancelStopDate: cancelReq.cancelDate,
                    IsStopped: false,
                    Status: 3
                })
            }else{
                updatedStop = await stopDB.update({
                    CancelStopDate: cancelReq.cancelDate,
                    IsStopped: false
                })
            }
            

            return res.status(200).json({
                message: "Cancel Stop Payment Successfully",
                data:{
                    StopPayment: updatedStop,
                    AffectedItems: updatedChequeItems[0]
                }
            })
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    enquiryCancelStop: async (req, res, next) => {
        try{
            const enquiryReq = {
                chequeID: req.body.chequeID,
                docID: req.body.docID,
                customerID: req.body.customerID,
                chequeType: req.body.chequeType,
                workingAccount: req.body.workingAccount,
                customerName: req.body.customerName,
                chequeNo: req.body.chequeNo,
                cancelDate: req.body.cancelDate
            }
            if(enquiryReq.chequeType == "AB"){
                return res.status(200).json({
                    message: "Enquiry Stop_Payment Sucessfully",
                    data: []
                })
            }else{
                let enquiryCondition = {}
                if(enquiryReq.docID)
                    enquiryCondition.DocID = {[Op.substring]: enquiryReq.docID}
                if(enquiryReq.customerID)
                    enquiryCondition.CustomerID = {[Op.substring]: enquiryReq.customerID}
                
                if(enquiryReq.workingAccount)
                    enquiryCondition.WorkingAccount = {[Op.substring]: enquiryReq.workingAccount}
                if(enquiryReq.customerName)
                    enquiryCondition.CustomerName = {[Op.substring]: enquiryReq.customerName}
                if(enquiryReq.chequeNo){
                    enquiryCondition.ChequeNoStart = {[Op.lte]: [enquiryReq.chequeNo]}
                    enquiryCondition.ChequeNoEnd = {[Op.gte]: [enquiryReq.chequeNo]}
                }

                if(enquiryReq.chequeID)
                    enquiryCondition.ChequeID = {[Op.substring]: enquiryReq.chequeID}
                    
                if(enquiryReq.cancelDate)
                    enquiryCondition.CancelStopDate = enquiryReq.cancelDate
                
                enquiryCondition.IsStopped = false

                console.log(enquiryCondition)

                const stopDB = await chequeStopModel.findAll({
                    where: enquiryCondition,
                    include: [{
                            model: chequeStopReasonModel
                        }]
                })

                if(!stopDB){
                    throw "Enquiry Failed"
                }

                return res.status(200).json({
                    message: "Enquiry Stop_Payment Sucessfully",
                    data: stopDB
                })
            }
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    }

}

module.exports = chequeStopController