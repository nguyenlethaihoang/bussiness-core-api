const chequeModel = require('../../models/cheque/cheque')
const chequeItemModel = require('../../models/cheque/chequeItem')
const chequeStopReasonModel = require('../../models/storage/cheque-stop-reason')
const chequeStopModel = require('../../models/cheque/stop-payment')
const debitAccountModel = require('../../models/account/debitAccount')
const customerModel = require('../../models/customer/customer')
const chequeReturnModel = require('../../models/cheque/return-payment')
const { Op } = require('sequelize')
const StatusType = require('../../models/signature/statusType')

const returnChequeController = {
    return: async (req, res, next)=>{
        try{
            const returnReq = {
                chequeID: req.body.chequeID,
                chequeNo: req.body.chequeNo,
                returnedDate: req.body.returnedDate,
                narrative: req.body.narrative
            }

            // CHECK REQUEST
            if(!returnReq.chequeID || !returnReq.chequeNo){
                throw "Cheque_Ref_ID and Cheque_No are required"
            }

            // CHECK CHEQUE
            const chequeDB = await chequeModel.findOne({
                where: {
                    ChequeID: returnReq.chequeID,
                },
                include: [{
                    model: debitAccountModel,
                    include: [{
                        model: customerModel, as: "Customer"
                    }]
                }]
            })
            if(!chequeDB){
                throw "Cheque is not found"
            }

            // CHECK CHEQUE STATUS
            const statusDB = chequeDB.getDataValue('Status')
            if(statusDB !== 2)
                throw "Invalid Cheque"

            const chequeNoStartDB = chequeDB.getDataValue("ChequeNoStart")
            const chequeNoEndDB = chequeDB.getDataValue("ChequeNoEnd")
            // CHECK CHEQUE ITEM
            if(returnReq.chequeNo < chequeNoStartDB || returnReq.chequeNo > chequeNoEndDB)
                throw "Invalid Cheque_No"

            // CHECK CHEQUE ITEM STATUS
            const chequeItemDB = await chequeItemModel.findOne({
                where: {
                    ChequeID: returnReq.chequeID,
                    ChequeNo: returnReq.chequeNo
                }
            })

            if(!chequeItemDB)
                throw "Cheque_Item Error"
            
            const itemStatusDB = chequeItemDB.getDataValue("ChequeStatus")
            if(itemStatusDB != "Available") {
                throw "Cheque Item cannot be returned because of its status"
            }  



            // CREATE RETURN RECORD
            const newReturnRecord = await chequeReturnModel.create({
                ChequeID: returnReq.chequeID,
                CustomerName: chequeDB.get("DEBITACCOUNT").get("Customer").GB_FullName,
                DocID: chequeDB.get("DEBITACCOUNT").get("Customer").DocID,
                WorkingAccount: chequeDB.getDataValue("WorkingAccount"),
                CustomerID: chequeDB.get("DEBITACCOUNT").get("Customer").RefID,
                ChequeNo: returnReq.chequeNo,
                ReturnedDate: returnReq.returnedDate,
                IsReturned: true,
                Narrative: returnReq.narrative,
                Cheque: chequeDB.getDataValue("id"),
                Status: 1
            })

            if(!newReturnRecord)
                throw "Create Return_Record Error"

            return res.status(200).json({
                message: "Create Return_Record Successfully",
                data: newReturnRecord
            })
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    // GET INFORMATION BEFORE RETURN ACTION
    getInfo: async (req, res, next)=>{
        try{
            const chequeIDReq = req.params.id
            if(!chequeIDReq){
                throw "Cheque_Ref_ID is required"
            }

            const chequeDB = await chequeModel.findByPk(chequeIDReq)
            if(!chequeDB){
                throw "Cheque is not found"
            }
            const chequeIDDB = chequeDB.getDataValue("ChequeID")
            //GET EXTRA INFO
            const usedItems = await chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Used"
                }
            })

            const holdItems = await  chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Available"
                }
            })

            const stoppedItems = await  chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Stopped"
                }
            })

            const returnedItems = await  chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Canceled"
                }
            })


            return res.status(200).json({
                message: "Get Returning Information Successfully",
                data: {
                    Cheque: chequeDB,
                    Used: usedItems,
                    Hold: holdItems,
                    Stopped: stoppedItems,
                    Returned: returnedItems
                }
            })
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    // GET INFORMATION OF A RETURNED RECORD
    getReturnedInfo: async (req, res, next) => {
        try{
            // Return Record ID
            const chequeIDReq = req.params.id

            if(!chequeIDReq){
                throw "Cheque_Return_Record ID is required"
            }
            
            const returnDB = await chequeReturnModel.findByPk(chequeIDReq)
            if(!returnDB){
                throw "Cheque_Return_Record is not found"
            }
            const chequeIDDB = returnDB.getDataValue("ChequeID")

            const chequeDB = await chequeModel.findOne({
                where:{
                    ChequeID: chequeIDDB
                }
            })
            if(!chequeDB){
                throw "Cheque is not found"
            }
            //GET EXTRA INFO
            const usedItems = await chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Used"
                }
            })

            const holdItems = await  chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Available"
                }
            })

            const stoppedItems = await  chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Stopped"
                }
            })

            const returnedItems = await  chequeItemModel.findAndCountAll({
                where: {
                    ChequeID: chequeIDDB,
                    ChequeStatus: "Canceled"
                }
            })


            return res.status(200).json({
                message: "Get Returning Information Successfully",
                data: {
                    Cheque: chequeDB,
                    Used: usedItems,
                    Hold: holdItems,
                    Stopped: stoppedItems,
                    Returned: returnedItems,
                    ReturnedInfo: returnDB
                }
            })
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    },
    validate: async (req, res, next)=>{
        try{
            const chequeReturnReq = req.params.id
            const statusReq = req.body.status


            if(!chequeReturnReq)
                throw "Cheque_Return_Record ID is required"

            // CHECK RETURN ITEM
            const chequeReturnDB = await chequeReturnModel.findByPk(chequeReturnReq)
            if(!chequeReturnDB)
                throw "Cheque_Return_Record is not found"
            // -- check status --
            const statusDB = chequeReturnDB.getDataValue("Status")
            if(statusDB != 1)
                throw "Validated"
            

            if(statusReq == 2){
                const chequeReturnItemDB = await chequeItemModel.findOne({
                    where: {
                        ChequeID: chequeReturnDB.getDataValue("ChequeID"),
                        ChequeNo: chequeReturnDB.getDataValue("ChequeNo")
                    }
                })
    
                if(!chequeReturnItemDB)
                    throw "Cheque_Retunred_Item is not found"
    
                if(chequeReturnItemDB.getDataValue("ChequeStatus") != "Available"){
                    throw "Cheque Item cannot be returned because of its status"
                }
                
                // CHANGE ITEM STATUS
                const updatedItem = await chequeReturnItemDB.update({
                    ChequeStatus: "Canceled"
                })
    
                // UPDATE RETURN_RECORD STATUS
                const updatedReturnCheque = await chequeReturnDB.update({
                    Status: 2
                })

                return res.status(200).json({
                    message: "Validate Successfully",
                    data: {
                        ChequeItem: updatedItem,
                        ReturnCheque: updatedReturnCheque
                    }
                })
            }else if(statusReq == 3){
                // UPDATE RETURN_RECORD STATUS
                const updatedReturnCheque = await chequeReturnDB.update({
                    Status: 3
                })

                return res.status(200).json({
                    message: "Validate Successfully",
                    data: {
                        ReturnCheque: updatedReturnCheque
                    }
                })
            }else{
                throw "Invalid Status "
            }
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }

    },
    enquiry: async (req, res, next)=> {
       try{
            const enquiryReq = {
                docID: req.body.docID,
                customerID: req.body.customerID,
                chequeType: req.body.chequeType,
                workingAccount: req.body.workingAccount,
                customerName: req.body.customerName,
                chequeNo: req.body.chequeNo,
                returnedDate: req.body.returnedDate,
                chequeID: req.body.chequeID
            }
            if(enquiryReq.chequeType == "AB"){
                return res.status(200).json({
                    message: "Enquiry Stop_Payment Sucessfully",
                    data: []
                })
            }else{
                let enquiryCondition = {}
                if(enquiryReq.docID)
                    enquiryCondition.DocID = {[Op.substring]: enquiryReq.docID}
                if(enquiryReq.customerID)
                    enquiryCondition.CustomerID = {[Op.substring]: enquiryReq.customerID}
                
                if(enquiryReq.workingAccount)
                    enquiryCondition.WorkingAccount = {[Op.substring]: enquiryReq.workingAccount}
                if(enquiryReq.customerName)
                    enquiryCondition.CustomerName = {[Op.substring]: enquiryReq.customerName}
                if(enquiryReq.chequeNo){
                    enquiryCondition.ChequeNo = enquiryReq.chequeNo
                }
                if(enquiryReq.chequeID)
                    enquiryCondition.ChequeID = {[Op.substring]: enquiryReq.chequeID}
                    
                if(enquiryReq.returnedDate)
                    enquiryCondition.returnedDate = enquiryReq.returnedDate
                
                enquiryCondition.IsReturned = true

                console.log(enquiryCondition)

                const returnedDB = await chequeReturnModel.findAll({
                    where: enquiryCondition,
                    include: [{
                        model: chequeModel
                    }]
                })

                if(!returnedDB){
                    throw "Enquiry Failed"
                }

                return res.status(200).json({
                    message: "Enquiry Returned_Payment Sucessfully",
                    data: returnedDB
                })
            }
        }catch(err){
            return res.status(404).json({
                message: err.message || err
            })
        }
    }
}

module.exports = returnChequeController

