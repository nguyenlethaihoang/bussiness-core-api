const signatureModel = require('../../models/signature/signature')
const signatureStatusModel = require('../../models/signature/statusType')
const customerModel = require('../../models/customer/customer')
const appError = require('../../utils/appError')
const asyncHandler = require('../../utils/async')
const minioClient = require('../../utils/minIO/minio-connect')

const bucketName = process.env.SIGNATURE_BUCKET_NAME


const signatureMinIOController = {

    // [GET] /signature/get_all
    getAll: asyncHandler(async (req, res, next) => {
       
    }),

    // [POST] /signature/upload
    upload: asyncHandler( async (req, res, next) => {
        //CHEKC SIGNATURE INFO
        const signatureReq = {
          customerID: req.body.customerID,
          description: req.body.description
        }

        if(!signatureReq.customerID){
          return next( new appError("Enter customer ID"))
        }
        console.log('customerID')
        console.log(signatureReq.customerID)
        const customerDB = await customerModel.findOne({
          where:{
            RefID: signatureReq.customerID
          }
          
        })
        .catch(err => {
            console.log(err)
            return next(new appError("Customer not found", 404))
        })

        //GET IMAGE
        const file = req.file
        if(!file){
            return next(new appError("Provide image", 404))
        }

        // STORE TO DATABASE
        const newSignature = await signatureModel.create({
          CustomerID: customerDB.getDataValue('id'),
          Description: signatureReq.description,
          Status: 1
        })

        // STORE BUCKET
        const objectName = file.originalname + "_" + newSignature.getDataValue('id');
        const submitFileDataResult = await minioClient
        .putObject(bucketName, objectName, file.buffer)
        .catch((e) => {
          console.log("Error while creating object from file data: ", e);
          throw e;
        });
    
        console.log("File data submitted successfully: ", submitFileDataResult);

        // UPDATE DATABASE IMAGE's URL
        const updatedSignature = await newSignature.update({
          URL: objectName
        })

        return res.status(200).json({
          message: 'uploaded',
          data: updatedSignature
        })
    }),

    // [GET] /signature/get/:signatureid
    getBySignatureID: asyncHandler( async (req, res, next) => {
        const signatureID = req.params.signatureid

        const signatureDB = await signatureModel.findByPk(signatureID, {
          include: [customerModel]
        })

        const URLString = ''
        minioClient.presignedUrl('GET', bucketName, signatureDB.getDataValue('URL'), 120, function(err, presignedUrl) {
            if (err) return console.log(err)
            URLString = presignedUrl
        })

        return res.status(200).json({
          message: "signature",
          data:{
              signatureData: signatureDB,
              URL: URLString
          }
      })
          
    }),

    // [GET] /signature/get_by_customer/:customerid
    getByCustomerID: asyncHandler( async (req, res, next) => {
        const customerDocID = req.params.docID
        if(!customerDocID){
          return next(new appError("Error", 404))
        }

        const customerDB = await customerModel.findOne({
          where: {DocID: customerDocID},
          attributes: ["id", "GB_ShortName", "GB_FullName"]
          })
          .catch(err => {
              console.log(err)
              return next(new appError("Customer not found", 404))
          })

        const customerIDDB = customerDB.getDataValue("id")
        const {count, rows} = await signatureModel.findAndCountAll({
            where: {CustomerID: customerIDDB }
        })

        let signaturesRes = []

        rows.map(signature => {
          let URLString = ''
          minioClient.presignedUrl('GET', bucketName, signature.getDataValue('URL'), 1200 , function(err, presignedUrl) {
            if (err) return console.log(err)
            URLString = presignedUrl
          })
          let signatureRes = {
              signature: signature,
              URL: URLString
          }
          signaturesRes.push(signatureRes)
        })

        return res.status(200).json({
            message: "signature",
            data: {
                quantity: count,
                customer: customerDB,
                signature: signaturesRes
            }
        })  
    }),

    // [GET] /signature/get_by_status/:status
    getByStatus: asyncHandler( async (req, res, next) => {
        
    }),

    // validate
    // [PUT] /signature/validate/:signatureid
    update: asyncHandler( async (req, res, next) => {
      const signatureReq = {
        status: req.body.status,
        description: req.body.description
      }

        const signatureID = req.params.signatureid

        if(!signatureReq.status){
            return next(new appError("Enter signature status"), 404)
        }

        const signatureDB = await signatureModel.findByPk(signatureID)
        if(!signatureDB){
            return next(new appError("Signature not found!", 404))
        }

        const newSignatureData = await signatureDB.update({
            Status: signatureReq.status,
            Description: signatureReq.description
        })

        let URLString = ''
        minioClient.presignedUrl('GET', bucketName, signatureDB.getDataValue('URL'), 120, function(err, presignedUrl) {
          if (err) return console.log(err)
          URLString = presignedUrl
        })

        return res.status(200).json({
            message: "update",
            data: {
                signature: newSignatureData,
                URL: URLString
            }
        })

    }),

    // [PUT] /signature/change_image/:signatureid
    changeSignatureImage: asyncHandler( async (req, res, next) => {
      const signatureIDReq = req.params.signatureid

      if(!signatureIDReq){
          return next( new appError("Provide signature ID"))
      }
          // GET SIGNATURE DB 
          const signatureDB = await signatureModel.findByPk(signatureIDReq)
          if(!signatureDB){
              console.log("Signature not found")
              return next(new appError("Signature not found", 404))
          }

          const oldImage = signatureDB.getDataValue('URL')

          // UPLOAD NEW SIGNATURE
          const file = req.file
          if(!file){
              return next(new appError("Provide image", 404))
          }

          const objectName = file.originalname + "_" + signatureIDReq;
          const submitFileDataResult = await minioClient
          .putObject(bucketName, objectName, file.buffer)
          .catch((e) => {
            console.log("Error while creating object from file data: ", e);
            throw e;
          });
          console.log("File data submitted successfully: ", submitFileDataResult);
          
          // UPDATE SIGNATURE DATA
          const updatedSignature = await signatureDB.update({
              URL: objectName,
              Status: 1
          })
      // REMOVE OLD SINGATURE ON CLOUD DB
      minioClient.removeObject(bucketName, oldImage)

      return res.status(200).json({
          message: "updated",
          data: updatedSignature
      })
    }),

    deleteImage: async (req, res, next) => {
      const signatureIDReq = req.params.id

      if(!signatureIDReq){
        return next( new appError("Provide signature ID"))
      }

      // GET FROM DB
      const signatureDB = await signatureModel.findByPk(signatureIDReq)
        if(!signatureDB){
            console.log("Signature not found")
            return next(new appError("Signature not found", 404))
        }

      const oldImage = signatureDB.getDataValue('URL')

      // REMOVE OLD SINGATURE ON CLOUD DB
      minioClient.removeObject(bucketName, oldImage)

      // REMOVE FROM DB
      await signatureDB.destroy();

      return res.status(200).json({
        message: "delete"
      })
        
    }
}

module.exports = signatureMinIOController