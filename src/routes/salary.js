const express = require("express")
const route = express.Router()
const multer = require('multer')
const inMemoryStorage = multer.memoryStorage()
const uploadStrategy = multer({ storage: inMemoryStorage }).single('file')
const salaryPaymentController = require('../controllers/salaryPayment')

route.post('/create', salaryPaymentController.create)
route.put('/validate/:ID', salaryPaymentController.validate)
route.get('/:ID', salaryPaymentController.getID)
route.post('/enquiry', salaryPaymentController.enquiry)
route.post('/upload',uploadStrategy, salaryPaymentController.uploadFile)

module.exports = route