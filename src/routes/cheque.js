const express = require("express")
const route = express.Router()

const issueChequeController = require('../controllers/cheque/issueCheque')
const withdrawalChequeController = require('../controllers/cheque/withdrawalCheque')
const transferChequeController = require('../controllers/cheque/transferCheque')
const stopPaymentController = require('../controllers/cheque/stop-payment-cheque')
const returnChequeController = require('../controllers/cheque/return-payment-cheque')

// ISSUE CHEQUE ROUTES
route.post('/issue', issueChequeController.issue)
route.get('/issue/:id', issueChequeController.getID)
route.post('/issue/enquiry', issueChequeController.enquiryCheque)
route.put('/issue/validate/:id', issueChequeController.validate)

// WITHDRALWAL CHEQUE ROUTES
route.post('/withdrawal', withdrawalChequeController.withdrawal)
route.put('/withdrawal/validate/:id', withdrawalChequeController.validate)
route.get('/withdrawal/get/:id', withdrawalChequeController.getID)
route.post('/withdrawal/enquiry', withdrawalChequeController.enquiry)

// TRANSFER CHEQUE ROUTES
route.post('/transfer', transferChequeController.transfer)
route.put('/transfer/validate/:id', transferChequeController.validate)
route.get('/transfer/get/:id', transferChequeController.getID)
route.post('/transfer/enquiry', transferChequeController.enquiry)

// STOP PAYMENT 
route.post('/stop', stopPaymentController.stopPayment)
route.get('/stop/:stopID', stopPaymentController.getStopPaymentID)
route.put('/stop/:stopID', stopPaymentController.validate)
route.post('/stop_enquiry', stopPaymentController.enquiry)
route.post('/cancel_stop_payment/:stopID', stopPaymentController.cancelStopPayment )
route.post('/cancel_stop_enquiry', stopPaymentController.enquiryCancelStop)

// RETURN PAYMENT
route.get('/return/get/:id', returnChequeController.getInfo)
route.post('/return', returnChequeController.return)
route.put('/return/:id', returnChequeController.validate)
route.get('/return/:id', returnChequeController.getReturnedInfo)
route.post('/return_enquiry', returnChequeController.enquiry)
module.exports = route